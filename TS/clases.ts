class Transporte{
    private velocidad: number;
    private formaDeMovilidad: string;

    constructor(velocidad: number, formaDeMovilidad: string){
        this.velocidad = velocidad;
        this.formaDeMovilidad = formaDeMovilidad; 
    }

    getVelocidad(){
        return this.velocidad;
    }

    getFormaDeVelocidad(){
        return this.formaDeMovilidad;
    }

    setVelocidad(velocidad: number){
        this.velocidad = velocidad;
    }

    setFormaDeVelocidad(formaDeMovilidad: string){
        this.formaDeMovilidad = formaDeMovilidad;
    }
}

const transporte: Transporte = new Transporte(20, 'tierra');

class Auto extends Transporte{
    private cantidadDePuertas: number;

    constructor(velocidad: number, formaDeMovilidad: string, cantidadDePuertas: number){
        super(velocidad, formaDeMovilidad);
        this.cantidadDePuertas = cantidadDePuertas;
    }
    getVelocidad(){
        return super.getVelocidad() + 10;
    }

    getCantidadDePuertas(){
        return this.cantidadDePuertas;
    }

    setCantidadDePuertas(cantidadDePuertas: number){
        this.cantidadDePuertas = cantidadDePuertas;
    }
}

const auto: Auto = new Auto(120, 'tierra', 5);