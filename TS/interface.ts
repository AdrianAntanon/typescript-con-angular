type Dni = number;

// Una interface es un tipo abstracto que va a denotar un contrato representando
// una estructura para nuestros objetos
// Por último, las interfaces no cuentan con constructor

interface Persona{
    altura: number;
    edad: number;
    nombre: string;
    apellido: string;
    dni: Dni;
}

// Si ponemos por ejemplo altura ?= number; lo que decimos es que ES OPCIONAL la altura.

const persona: Persona = {
    altura: 1.88,
    edad: 26,
    nombre: 'Adri',
    apellido: 'Antañón',
    dni: 39444363,
}