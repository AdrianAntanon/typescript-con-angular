// Si en las Union Types era un O aquí es un I, es decir
// el type que creamos tiene que tener todo lo que indicamos
// En este caso tanto prop1 como prop2

interface Interface1{
    prop1: number;
}

interface Interface2{
    prop2: number;
}

type InterfaceMix2 = Interface1 & Interface2; 

const interfaceMix: InterfaceMix2 = {
    prop1: 1,
    prop2: 10
} 