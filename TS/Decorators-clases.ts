function init(target){
    return class extends target{
        nombre = 'Adri';
        apellido = 'Antañón';
        sayMyName2(){
            return `${this.nombre} ${this.apellido}`;
        }
    }
}

// @init, si quiero indicar que esto es un decorator
class P{
    constructor(){}
    sayMyName2(){}
}

const p: P = new P();

console.log(p.sayMyName2());