// APLICACIÓN DE PROPIEDADES

// vamos a sobreescribir los setters y getters por nuevas propiedades
function logProperty(target, key){
    // vamos a obtener el elemento que llega
    let _val = this[key];
    // crearemos los nuevos getters y setters para sobreescribir
    const getter = () => {
        console.log(`Get: ${key} => ${_val}`);
        return _val;
    }
    const setter = (newValue) => {
        console.log(`Set: ${key} => ${newValue}`);
        _val = newValue;
    }

    // los guardamos en un objeto
    const ObjectProperty = {
        get: getter,
        set: setter
    }
    
    // sobreescribimos las propiedades 
    Object.defineProperty(target, key, ObjectProperty);
}

class Persona{
    /* @logProperty */
    public name: string;

    constructor(name: string){
        this.name = name;
    }
}

const a = new Persona('Adri');
a.name = 'Adrián'; // Set: name => 'Adrián'
const nameFromClass = a.name; // Get: name => 'Adrián'