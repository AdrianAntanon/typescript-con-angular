/* Shapes es una forma de comparar dos clases y saber si tienen las mimas propiedades o si una exíste una relacion hijo-padre entre ellas. Por ejemplo, un pediatra es un médico mas no necesariamente un médico debe ser un pediatra */

class Persona{
    private anyos: number;
    private largo: number;
    private num_dni: string;

    constructor(anyos: number, largo: number, num_dni: string){
        this.anyos = anyos;
        this.largo = largo;
        this.num_dni = num_dni;
    }
}

class Alumno extends Persona{
    private matricula: string;

    constructor(anyos: number, largo: number, num_dni: string, matricula: string){
        super(anyos, largo, num_dni);
        this.matricula = matricula;
    }
}

let adri: Persona = new Persona(26, 1.88, '39444363F');
let adrian: Alumno = new Alumno(26, 1.88, '39444363F', 'DAW');


adri = adrian;
// adrian = adri; 